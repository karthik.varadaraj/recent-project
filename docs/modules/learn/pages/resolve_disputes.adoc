:imagesdir: ./images

=== Resolving Disputes

Disputes are resolved if follow-up action is created for a dispute. Only users with the appropriate level of security are allowed to manage disputes.

To resolve a dispute:

. Click _Cases_ on the primary navigation menu.
. Retrieve the required case using filters.
. Open the case.
. On the _Case Panel_, click *WORK ON CASE* button.
. Select the _Dispute_ on the _Issues_ tab. 
. Click the _Resolve Issue_ icon.
. Select the _Resolution Code_ from the dropdown.
. Enter the _Resolution Text_.
. Enter the amount.
. Click the *Confirm* button.