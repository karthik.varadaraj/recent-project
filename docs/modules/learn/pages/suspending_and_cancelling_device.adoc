:imagesdir: ./images

=== Suspending and Cancelling Device

A customer can decide to suspend or cancel the device depending on their requirements.

To suspend a device:

. Open the _Device_ tab.
. Select the active device.

    TIP: Enabled toggle button indicates the active status of the device.

. Click the *Suspend* button.
. Select the _Reason for this change_.
. Click the *Suspend Plan* button.

To cancel a device:

. Open the _Device_ tab.
. Select the active device.
. Click the toggle button.
. Select the _Reason for this change_.
. Select the _Cancellation Reason_.
. Click the *Cancel Plan* button.