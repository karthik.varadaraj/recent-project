:imagesdir: ./images

== Managing Payments

* A payment made by a customer is recorded as a credit against the customer’s account. 
* A payment can be recorded against only one customer’s account, and optionally against one or more invoices. 
* A payment received from a customer when there are no outstanding invoices is recorded as a credit balance.

Payments appear in the customer’s account details, statements, and invoices generated for the customer. 

This section describes how to create and manage payments received from customers.