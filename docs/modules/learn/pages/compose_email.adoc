:imagesdir: ./images

=== Composing Email

To compose an email from the case panel:

. Retrieve the case using the _Global Search_ bar.
. Click the *View* icon to open the case panel.
. Click the *WORK ON CASE* button.
. Select the _Email_ tab.
. Click the *Compose* button.
. Enter the content in the email body.
. Attach files depending on the requirement.
. Click the *Send* button.

:note-caption: TIP

[NOTE]
====
* Default To address can be replaced, and additional email addresses can be manually entered.
* Default Subject line can be replaced by entering the subject line manually.
* To add email copies, click Cc and Bcc.
* Bold, Italics, and Underline can be used to format the content entered in the email body.
====

