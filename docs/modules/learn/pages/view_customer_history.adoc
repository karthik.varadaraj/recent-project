:imagesdir: ./images

=== Viewing Customer History

Customer history can be retrieved whenever a user wants to view the history of cases, activity, and comments, etc of a particular customer.

To view the customer history:

. Search for the customer using the _Global Search_ bar.
. Select the customer.
. Click the *View* icon.
. Select the associated contact.
. Click the *Validate Contact* button.
. Validate the contact details using _Finger ID Validation_ or _Security Questions_. 

    NOTE: The user can skip the validation by clicking the Skip Validation link.

. On the secondary navigation menu, click _History_.
. Issues are listed in the _Cases_ tab.

    * Recent issues are listed on the top, and the user can filter the issues based on the timeline. 
    * Click the *Select* button to add filters.
    * Filters can be applied depending on the requirements. For example, the filters can be applied for the present month, previous month, or present-day, etc.
    * Click the *Confirm* button after applying the filters. 

. Activities are listed in the _Activity_ tab.
. All the communications with the customer are listed in the _Comments_ tab.