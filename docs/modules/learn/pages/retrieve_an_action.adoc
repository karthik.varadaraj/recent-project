:imagesdir: ./images

=== Retrieving an Action

An action is retrieved when the user intends to complete, cancel, or reassign it.

To retrieve all open actions assigned to the user or user's workgroup:
    
. Open the _CSR Dashboard_.

    TIP:
    All the open actions assigned to the user or user's workgroup are listed on the CSR Dashboard.
    The number of actions that have due dates is displayed to the user.

. Click the _See All_ link.
. Click the _View All_ button to expand the actions panel.
. Apply the filters.
. Select an action from the list using the checkbox.