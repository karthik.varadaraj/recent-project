:imagesdir: ./images

=== Creating a One-off Charge

A one-off charge is created to apply charges on customer service or to the overall account. For example, if the customer requests a bill reprint, the service provider may apply a specific charge for this activity.

If a negative amount is entered, the user's adjustment authorisation limit may prevent the charge from being created. 

To create a one-off charge:

. Retrieve the customer using the _Global Search_ bar.
. Select the customer from the _Customer Results_.
. Click the _View_ icon.
. Choose the associated contact.
. Click the *Validate Contact* button.
. Validate the contact details using _Finger ID Validation_ or _Security Questions_. 

    NOTE: The user can skip the validation by clicking the Skip Validation link.

. On the secondary navigation menu, click _Billing_.
. Click the _Account Actions_ dropdown.
. Select _ONE-OFF CHARGES_.
. Enter the amount.
. Select the _Reason for One-off charge_ from the dropdown.
. Enter a note.
. Click the *Create Charge* button.