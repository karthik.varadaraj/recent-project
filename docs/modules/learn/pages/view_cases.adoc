:imagesdir: ./images

=== Viewing Case

A case number is auto-generated for a set of issues, actions, orders, and payment requests associated with a contact or customer.

To view a case:

. Select _Cases_ from the dropdown of the _Global Search_ bar.

    NOTE: Case ID search parameter is auto-selected.

. Enter the _Case ID_ and click the search icon.
. Click the _View_ icon to view the case details in a case panel.