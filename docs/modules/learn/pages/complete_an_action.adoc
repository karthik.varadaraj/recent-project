:imagesdir: ./images

=== Completing an Action

Action is considered complete when a user performs the required task or procedure. A case cannot be closed until all the associated actions are completed or canceled.

To complete an action:

* Get the next action, or retrieve an action.